package com.iss.iescp.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

/**
 * 描述: 文件下载工具类
 */
public class FileDownLoadUtil {
	
	/**
	 * 下载文件覆盖标志
	 */
	public final static boolean FLAG_0 = false;
	public final static boolean FLAG_1 = true;
	
	/**
	 * 通过URL下载文件 <br><pre>
	 */
	public static String downLoadFromUrl(String urlStr,String fileName,String savePath,boolean flag) {
		FileOutputStream fos = null;
		InputStream inputStream = null;
		
		URL url = null;
		try {
			url = new URL(urlStr);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			
			//设置超时间为10秒
			conn.setConnectTimeout(10*1000);
			
			//防止屏蔽程序抓取而返回403错误
			conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
			
			//得到输入流
			inputStream = conn.getInputStream();
			
			//获取自己数组
			byte[] getData = readInputStream(inputStream);
			
			File saveDir = new File(savePath);
			
			// 不存在则建立目录
			if(!saveDir.exists()) {
				saveDir.mkdir();
			} else {
				// 文件更新的情况下则会新建目录
				if(flag) {
					saveDir = new File(savePath+new Date().getTime());
					savePath = savePath+new Date().getTime()+"/";
					saveDir.mkdir();
				}
			}
			
			File file = new File(saveDir+"/"+fileName);
			fos = new FileOutputStream(file);   
			
			fos.write(getData);
			
		} catch (MalformedURLException e) {
			LogUtil.error("文件下载异常：{}", e, e.getMessage());
			
		} catch (IOException e) {
			
			LogUtil.error("文件下载异常：{}", e, e.getMessage());
		} finally {
			if(fos!=null){
				try {
					fos.close();
				} catch (IOException e) {
					LogUtil.error("输出流关闭异常：{}", e, e.getMessage());
				}  
			}
			if(inputStream!=null){
				try {
					inputStream.close();
				} catch (IOException e) {
					LogUtil.error("输入流关闭异常：{}", e, e.getMessage());
				}
			}
		}
		return savePath;
	}
	
	/**
	 * 通用文件下载功能
	 */
	public static String downLoadFromUrl(String urlStr,String fileName,String savePath) {
		return downLoadFromUrl(urlStr,fileName,savePath,FLAG_0);
	}
	
	/**
	 * 获取流字节数组
	 */
	public static  byte[] readInputStream(InputStream inputStream) throws IOException {  
		byte[] buffer = new byte[1024];  
		int len = 0;  
		ByteArrayOutputStream bos = new ByteArrayOutputStream();  
		while((len = inputStream.read(buffer)) != -1) {  
			bos.write(buffer, 0, len);  
		}  
		bos.close();  
		return bos.toByteArray();  
	}

	public static void main(String[] args) {
		downLoadFromUrl("http://127.0.0.1:8080/downLoadFile?id=1",new Date().getTime()+".jpg","D:/file/");
		
	}
}
