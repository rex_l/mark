package com.iss.iescp.utils;

import java.io.StringWriter;
import java.util.Map;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VelocityUtils {
    //初始化日志对象
  	private static Logger log = LoggerFactory.getLogger(VelocityUtils.class);
  	
    static {
        try {
            Velocity.init(IConstants.VELOCITY_PROPERTIES);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            log.error(e.getMessage(), e);
            //e.printStackTrace();
        }
    }

    public static String mergeTemplate(Map<String, Object> contextmap,
                                       String templatepath) {
        StringWriter sw = new StringWriter(1000);
        try {

            Template template = Velocity.getTemplate(templatepath, "UTF-8");
            Context context = new VelocityContext(contextmap);
            template.merge(context, sw);
            //sw.flush();
        } catch (Exception ex) {
        	log.error(ex.getMessage(), ex);
            //ex.printStackTrace();
        }
        return sw.toString();
    }


}
