package com.demo.velocity;

import java.util.Date;


/**
 * ClassName：Utils<br>
 * Description：通用工具类        .<br>
 * <p>company：58同城 <br>
 * Copyright：Copyright © 2011 58.com All Rights Reserved<br>
 *
 * @author auto
 * @Date 2011-7-30 21:47:50
 * @since JRE 1.6.0_22  or higher
 */
public interface IConstants {

    String BASE_DIR = System.getProperty("user.dir");
    String SYSTEM_GLOBALS_FILE = "SystemGlobals.properties";
    String SYMBOL_SLASH = "/";
    String RESOURCES_PATH = "src/main/resources";
    //String LOG4J_PROPERTIES = BASE_DIR + SYMBOL_SLASH + "config/log4j.properties";
    String SYSTEM_GLOBAL_PROPERTIES = BASE_DIR + SYMBOL_SLASH + RESOURCES_PATH + SYMBOL_SLASH + SYSTEM_GLOBALS_FILE;
    String VELOCITY_PROPERTIES = BASE_DIR + SYMBOL_SLASH +  RESOURCES_PATH + SYMBOL_SLASH + "velocity.properties";

}