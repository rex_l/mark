import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreeUtil {
    /**
     * 找出集合中所有的父子关系
     * @param List<Map<String, Object>> list 对象集合
     * @param String field 父节点主键key
     * @return Map<父节点主键, List<Map<父节点主键,子节点集合>>>
     */
    private Map<String, List<Map<String, Object>>> bulidTreeListMap(List<Map<String, Object>> list, String field) {
        Map<String, List<Map<String, Object>>> childMap = new HashMap<>();
        if (ListUtil.isNotEmpty(list) && StringUtil.isNotBlank(field)) {
            for (Map<String, Object> map : list) {
                String parentId = null;
                parentId = map.get(field) == null?"":String.valueOf(map.get(field));
                if (childMap.get(parentId) == null) {
                    List<Map<String, Object>> lst = new ArrayList<>();
                    lst.add(map);
                    childMap.put(parentId, lst);
                } else {
                    childMap.get(parentId).add(map);
                }
            }
        }
        return childMap;
    }

    /**
     * 递归排列树形集合
     * @param Map<String, List<Map<String, Object>>> treeMap 父子结构节点集合
     * @param String parentId 筛选顶级父节点ID
     * @param String idKey 父节点主键key
     * @return List<Map<String, Object>> 父子结构树形集合
     */
    private List<Map<String, Object>> queryChildrenList(Map<String, List<Map<String, Object>>> treeMap,String parentId, String idKey) {
        List<Map<String, Object>> treeList = treeMap.get(parentId);
        List<Map<String, Object>> ret = new ArrayList<>();
        if (ListUtil.isNotEmpty(treeList)) {
            for (Map<String, Object> tree : treeList) {
                // 调用递归方法，计算子节点
                List<Map<String, Object>> childrenList = queryChildrenList(treeMap, tree.get(idKey).toString(),idKey);
                // 组装Map集合
                if (!childrenList.isEmpty()) {
                    tree.put("children", childrenList);
                }
                ret.add(tree);
            }
        }
        return ret;
    }

    /**
     * 递归排列树形集合
     * @param Map<String, List<Map<String, Object>>> treeMap 父子结构节点集合
     * @param String parentId 筛选顶级父节点ID
     * @param String idKey 父节点主键key
     * @return List<String>> 子节点主键集合
     */
    private List<String> queryChildrenStringToList(Map<String, List<Map<String, Object>>> treeMap,String parentId, String idKey) {
        List<Map<String, Object>> treeList = treeMap.get(parentId);
        List<String> ret = new ArrayList<>();
        if (ListUtil.isNotEmpty(treeList)) {
            for (Map<String, Object> tree : treeList) {
                // 调用递归方法，计算子节点
                ret.addAll(queryChildrenStringToList(treeMap, tree.get(idKey).toString(),idKey));
                ret.add(tree.get(idKey).toString());
            }
        }
        return ret;
    }
}
