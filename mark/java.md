### 目录
* [Base](#base)
  * [运算符](#operator)
  * [JAVA8](#java8)
  * [Thread](#thread)
  * [Collection](#collection)
  * [Map](#map)
  * [GC](#GC)
* [优化](#optimize)
  * [jvm参数](#jvmParam)
  * [内存溢出三种情况](#oom)
* [Spring](#spring)
  * [事务传播机制](#transaction)
* [SpringBoot](#springboot)
* [SpringCloud](#springcloud)
---
### **Base**  <span id = "base" />

#### 运算符  <span id = "operator" />
[Java位移运算](https://blog.csdn.net/mryang125/article/details/109409864)
```java
/**
位移运算
    1. << 左移位运算  n<<k == n*2^k
        <<左移无论负数还是正数，在低位永远补0
    2. >> 算数右移  (正)n>>k == n/2^k
        >>是带符号右移。负数高位补1，正数补0         ----->>>也就是在高位用符号位进行填充。
    2. >>> 无符号右移  (正)n>>k == n/2^k
        >>>是不带符号右移，不论负数还是正数，高位补0（ 无符号右移。忽略符号位。空位都以0补齐）
在位运算之前都需要将运算的数字转为二进制数
    1、与（&）：两个数都数为1，则该位结果为1，否则为0。
    2、非（~）：数为1，结果为0；数为0，结果为1。
    3、或（|）：两个数至少一个1，则运算结果为1；否则结果为0。
    4、异或（^）：两个数相同则结果为0，不同则为1
*/
```

#### JAVA8  <span id = "java8" />
1. Stream
   ```java
        List<Demo> retList = list.stream().filter(demo -> {
            // 过滤信息
            return demo.getA().equals("A");
        //}).map(demo -> {
            // .map()需要返回值
            // return demo; // 可以返回其他对象
        }).peek(demo -> {
            // .peek()处理内部对象数据
            if(demo.getA().equals("A")) {
                // 统一修改值
                demo.setA("updateA");
            }
        }).collect(Collectors.toList()); // 收集为集合类型
    ```
2. 函数式编程 Function
    ```java
        private static <T,R> R apply(T t, Function<T, R> mapper){
            return t!=null?mapper.apply(t):null;
        }
        Demo demo1 = new Demo();
        demo1.setA("aaa");
        Map<String,Object> testMap = new HashMap<String,Object>();
        testMap.put("1",apply(demo1,Demo::getA)); // aaa
        testMap.put("2",apply(demo1, i->demo1.getA())); // aaa
    ```

#### Thread  <span id = "thread" />
1. `Thread.run()`只是普通方法调用，并不会开线程（同步），等待方法执行完继续；
2. `Thread.start()`调用多线程方法（异步）；
* 线程池
  * 线程池执行流程
  * ![](./img/threadExecuteFlow.png)
  * 执行任务，核心线程数未达到corePoolSize该值，就创建线程。否则塞入任务队列。
  * 任务队列满了，就创建线程，但是总线程数不能超过该值maximumPoolSize。
  * 如果任务队列满了，线程数达到maximumPoolSize值，则执行失败策略。
  * 工作线程则不停的轮询去队列中poll任务，如果poll为空，则工作线程执行结束(回收线程)。
  * 如果工作线程数<=核心线程数corePoolSize，则使用take从队列中获取任务(核心线程一直await)。
* [ExecutorService 线程池详解](https://www.cnblogs.com/simpleDi/p/11342440.html)
* [ScheduledExecutorService原理解析](https://blog.csdn.net/qq_28666081/article/details/108811927)
```java
public ThreadPoolExecutor(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue,
                              ThreadFactory threadFactory,
                              RejectedExecutionHandler handler);
// 无返回的执行任务
executorService.execute(@NotNull Runnable runnable);
// 有返回的执行任务
executorService.submit(@NotNull Callable callable);
```
1. corePoolSize : 核心线程数，一旦创建将不会再释放。如果创建的线程数还没有达到指定的核心线程数量，将会继续创建新的核心线程，直到达到最大核心线程数后，核心线程数将不在增加；如果没有空闲的核心线程，同时又未达到最大线程数，则将继续创建非核心线程；如果核心线程数等于最大线程数，则当核心线程都处于激活状态时，任务将被挂起，等待有空闲线程时再执行。
2. maximumPoolSize : 最大线程数，允许创建的最大线程数量。如果最大线程数等于核心线程数，则无法创建非核心线程；如果非核心线程处于空闲时，超过设置的空闲时间，则将被回收，释放占用的资源。
3. keepAliveTime : 也就是当线程空闲时，所允许保存的最大时间，超过这个时间，线程将被释放销毁，但只针对于非核心线程。
4. unit : 时间单位，TimeUnit.SECONDS等。
5. workQueue : 任务队列，用于保存等待执行的任务的阻塞队列。可以选择以下几个阻塞队列。
   1. ArrayBlockingQueue：是一个基于数组结构的有界阻塞队列，必须设置容量。此队列按 FIFO（先进先出）原则对元素进行排序。
   2. LinkedBlockingQueue：一个基于链表结构的阻塞队列，可以设置容量，此队列按FIFO （先进先出） 排序元素，吞吐量通常要高于ArrayBlockingQueue。
   3. SynchronousQueue：一个不存储元素的阻塞队列。每个插入offer操作必须等到另一个线程调用移除poll操作，否则插入操作一直处于阻塞状态，吞吐量通常要高于LinkedBlockingQueue。
   4. PriorityBlockingQueue：一个具有优先级的无限阻塞队列。
6. threadFactory :  线程工厂，用于创建线程。
7. handler : 当线程边界和队列容量已经达到最大时，用于处理阻塞时的方法。


#### Collection  <span id = "collection" />
![](./img/Collection.png)
* ArrayList和LinkedList   
   (1) ArrayList和LinkedList数据结构的不同。ArrayList是基于数组实现的，LinkedList是基于双链表实现的。  
   (2) ArrayList底层的实现是Array，数组动态扩容实现，默认初始大小为10，添加元素时判断当前数组长度是否够存，不够存默认将扩容至原来容量的 1.5 倍，``NewLength=OldLength+OldLength>>1;``    
   因为Array是基于索引(index)的数据结构，它使用索引在数组中搜索和读取数据是很快的，可以直接返回数组中index位置的元素，因此在随机访问集合元素上有较好的性能。
   Array获取数据的时间复杂度是O(1),但是要插入、删除数据却是开销很大的，因为这需要移动数组中插入位置之后的的所有元素。  
   (3) LinkedList的随机访问集合元素时性能较差，因为需要在双向列表中找到要index的位置，再返回；但在插入，删除操作是更快的。因为LinkedList不像ArrayList一样，不需要改变数组的大小，也不需要在数组装满的时候要将所有的数据重新装入一个新的数组，这是ArrayList最坏的一种情况，时间复杂度是O(n)，而LinkedList中插入或删除的时间复杂度仅为O(1)。ArrayList在插入数据时还需要更新索引（除了插入数组的尾部）。  
   (4) LinkedList需要更多的内存，因为ArrayList的每个索引的位置是实际的数据，而LinkedList中的每个节点中存储的是实际的数据和前后节点的位置。  
   * 使用场景：  
   (1)如果应用程序对数据有较多的随机访问，ArrayList对象要优于LinkedList对象；
   (2)如果应用程序有更多的插入或者删除操作，较少的随机访问，LinkedList对象要优于ArrayList对象；
   (3)不过ArrayList的插入，删除操作也不一定比LinkedList慢，如果在List靠近末尾的地方插入，那么ArrayList只需要移动较少的数据，而LinkedList则需要一直查找到列表尾部，反而耗费较多时间，这时ArrayList就比LinkedList要快。
* ArrayList线程不安全()
    ```java
    public boolean add(E e) {
        /**
         * 添加一个元素时，做了如下两步操作
         * 1.判断列表的capacity容量是否足够，是否需要扩容
         * 2.真正将元素放在列表的元素数组里面
         */
        ensureCapacityInternal(size + 1);
        elementData[size++] = e;
        return true;
    }
    ```
    1. 在多个线程进行add操作时可能会导致elementData数组越界。
    具体逻辑如下：  
     (1) 列表大小为9，即size=9  
     (2) 线程A开始进入add方法，这时它获取到size的值为9，调用ensureCapacityInternal方法进行容量判断。  
     (3) 线程B此时也进入add方法，它获取到size的值也为9，也开始调用ensureCapacityInternal方法。  
     (4) 线程A发现需求大小为10，而elementData的大小就为10，可以容纳。于是它不再扩容，返回。  
     (5) 线程B也发现需求大小为10，也可以容纳，返回。  
     (6) 线程A开始进行设置值操作， elementData[size++] = e 操作。此时size变为10。  
     (7) 线程B也开始进行设置值操作，它尝试设置elementData[10] = e，而elementData没有进行过扩容，它的下标最大为9。于是此时会报出一个数组越界的异常ArrayIndexOutOfBoundsException.

    ```java
    elementData[size] = e;
    size = size + 1;
    ```
    2. 一个线程的值覆盖另一个线程添加的值，在单线程执行这两条代码时没有任何问题，但是当多线程环境下执行时，可能就会发生一个线程的值覆盖另一个线程添加的值，具体逻辑如下：  
    (1) 列表大小为0，即size=0;  
    (2) 线程A开始添加一个元素，值为A。此时它执行第一条操作，将A放在了elementData下标为0的位置上。  
    (3) 接着线程B刚好也要开始添加一个值为B的元素，且走到了第一步操作。此时线程B获取到size的值依然为0，于是它将B也放在了elementData下标为0的位置上。  
    (4) 线程A开始将size的值增加为1  
    (5) 线程B开始将size的值增加为2  
    (6) list[0]=B,list[1]=null;  
* 线程安全的List
    1. 目前比较常用的构建线程安全的List有三种方法：  
    (1)使用Vector容器
    (2)使用Collections的静态方法synchronizedList(List< T> list)
    (3)采用CopyOnWriteArrayList容器

#### Map  <span id = "map" />
* HashMap  
    1. HashMap的初始化大小是16,默认的负载因子大小为0.75，当一个map填满了75%的bucket时候，扩容为原来容器的两倍   
       ``newThr = oldThr << 1;`` 
* HashMap和HashTable
    1. HashTable中的方法是同步的，而HashMap中的方法在缺省情况下是非同步的。在多线程并发的情况下，可以直接使用HashTable，但要使用HashMap则需要手动增加同步处理。  
    2. HashTable中key和value都不允许出现null值。
* 遍历Map的方法
1. 使用迭代器
```java
    // 1.Map.Entry（效率高）
    Map map = new HashMap();
    Iterator iter = map.entrySet().iterator();
    while (iter.hasNext()) {
        Map.Entry entry = (Map.Entry) iter.next();
        Object key = entry.getKey();
        Object val = entry.getValue();
    }
    // 2.Object（效率低）
    Map map = new HashMap();
    Iterator iter = map.keySet().iterator();
    while (iter.hasNext()) {
        Object key = iter.next();
        Object val = map.get(key);
    }
```
2. for each
```java
    // 1.keySet()
    Map<String, String> map = new HashMap<String, String>();
    for (String key : map.keySet()) {
        map.get(key);
    }
    // 2.Entry
    Map<String, String> map = new HashMap<String, String>();
    for (Entry<String, String> entry : map.entrySet()) {
        entry.getKey();
        entry.getValue();
    }
```

#### GC  <span id = "GC" />

* 垃圾回收算法
* [Java的垃圾回收机制及算法](https://www.cnblogs.com/TheGCC/p/14738156.html)
  1. 标记清除算法
  * 使用通过可达性分析分析方法标记出需要回收的对象，删除所标记的对象；它的一个显著问题是一段时间后，内存会出现大量碎片，导致虽然碎片总和很大，但无法满足一个大对象的内存申请，从而导致 OOM，而过多的内存碎片（需要类似链表的数据结构维护），也会导致标记和清除的操作成本高，效率低下。
    ![](./img/GCbjqc.jpg)
  2. 标记整理算法
  * 标记整理法，知识在标记清除的基础上，追加了碎片的散落问题，在清除之后进行了碎片的整理，合并成一整块区域，但副作用是增了了GC的时间。
    ![](./img/GCbjzl.jpg)
  3. 复制
  * 为了解决标记清除算法的效率问题，有人提出了复制算法。它将可用内存一分为二，每次只用一块，当这一块内存不够用时，便触发 GC，将当前存活对象复制(Copy)到另一块上，将第一块儿内存整个清除掉
    ![](./img/GCbjfz.jpg)
  4. 分代收集
  * 将内存按照 Java 生存时间分为 新生代(Young) 和 老年代(Old)，
  * 对象一般都是先在 Eden区创建
  * 当Eden区满，触发 Young GC，此时将 Eden中还存活的对象复制到 S0中，并清空 Eden区后继续为新的对象分配内存
  * 当Eden区再次满后，触发又一次的 Young GC，此时会将 Eden和S0中存活的对象复制到 S1中，然后清空Eden和S0后继续为新的对象分配内存
  * 每经过一次 Young GC，存活下来的对象都会将自己存活次数加1，当达到一定次数后，会随着一次 Young GC 晋升到 Old区
  * Old区也会在合适的时机进行自己的 GC
    ![](./img/GCfd.jpg)
---
### **优化**  <span id = "optimize" />

#### jvm参数  <span id = "jvmParam" />
* -Xms：java Heap初始大小， 默认是物理内存的1/64。
* -Xmx：ava Heap最大值，不可超过物理内存。
* -Xmn：young generation的heap大小，一般设置为Xmx的3、4分之一 。增大年轻代后,将会减小年老代大小，可以根据监控合理设置。
* -Xss：每个线程的Stack大小，而最佳值应该是128K,默认值好像是512k。
* -XX:PermSize：设定内存的永久保存区初始大小，缺省值为64M。
* -XX:MaxPermSize：设定内存的永久保存区最大大小，缺省值为64M。
* -XX:SurvivorRatio：Eden区与Survivor区的大小比值,设置为8,则两个Survivor区与一个Eden区的比值为2:8,一个Survivor区占整个年轻代的1/10
* -XX:+UseParallelGC：F年轻代使用并发收集,而年老代仍旧使用串行收集.
* -XX:+UseParNewGC：设置年轻代为并行收集,JDK5.0以上,JVM会根据系统配置自行设置,所无需再设置此值。
* -XX:ParallelGCThreads：并行收集器的线程数，值最好配置与处理器数目相等 同样适用于CMS。
* -XX:+UseParallelOldGC：年老代垃圾收集方式为并行收集(Parallel Compacting)。
* -XX:MaxGCPauseMillis：每次年轻代垃圾回收的最长时间(最大暂停时间)，如果无法满足此时间,JVM会自动调整年轻代大小,以满足此值。
* -XX:+ScavengeBeforeFullGC：Full GC前调用YGC,默认是true。

#### 内存溢出三种情况  <span id = "oom" />
1. JVM Heap（堆）溢出：java.lang.OutOfMemoryError: Java heap space
* JVM在启动的时候会自动设置JVM Heap的值， 可以利用JVM提供的-Xmn -Xms -Xmx等选项可进行设置。Heap的大小是Young Generation 和Tenured Generaion 之和。在JVM中如果98%的时间是用于GC,且可用的Heap size 不足2%的时候将抛出此异常信息。
* 解决方法：手动设置JVM Heap（堆）的大小。
2. PermGen space溢出： java.lang.OutOfMemoryError: PermGen space
* PermGen space的全称是Permanent Generation space,是指内存的永久保存区域。为什么会内存溢出，这是由于这块内存主要是被JVM存放Class和Meta信息的，Class在被Load的时候被放入PermGen space区域，它和存放Instance的Heap区域不同，sun的 GC不会在主程序运行期对PermGen space进行清理，所以如果你的APP会载入很多CLASS的话，就很可能出现PermGen space溢出。一般发生在程序的启动阶段。
* 解决方法： 通过-XX:PermSize和-XX:MaxPermSize设置永久代大小即可。
3. 栈溢出： java.lang.StackOverflowError : Thread Stack space
* 栈溢出了，JVM依然是采用栈式的虚拟机，这个和C和Pascal都是一样的。函数的调用过程都体现在堆栈和退栈上了。调用构造函数的 “层”太多了，以致于把栈区溢出了。 通常来讲，一般栈区远远小于堆区的，因为函数调用过程往往不会多于上千层，而即便每个函数调用需要 1K的空间（这个大约相当于在一个C函数内声明了256个int类型的变量），那么栈区也不过是需要1MB的空间。通常栈的大小是1-2MB的。通俗一点讲就是单线程的程序需要的内存太大了。 通常递归也不要递归的层次过多，很容易溢出。
* 解决方法：1：修改程序。2：通过 -Xss: 来设置每个线程的Stack大小即可。


---
### **Spring**  <span id = "spring" />

#### 事务传播机制  <span id = "transaction" />
* [spring七种事务传播行为](https://zhuanlan.zhihu.com/p/436332667)
* required（默认）：支持使用当前事务，如果当前事务不存在，创建一个新事务。
* supports：支持使用当前事务，如果当前事务不存在，则不使用事务。
* mandatory：中文翻译为强制，支持使用当前事务，如果当前事务不存在，则抛出Exception。
* requires_new：创建一个新事务，如果当前事务存在，把当前事务挂起。
* not_supported：无事务执行，如果当前事务存在，把当前事务挂起。
* never：无事务执行，如果当前有事务则抛出Exception。
* nested：嵌套事务，如果当前事务存在，那么在嵌套的事务中执行。如果当前事务不存在，则表现跟REQUIRED一样。
    * 嵌套事务一个非常重要的概念就是内层事务依赖于外层事务。外层事务失败时，会回滚内层事务所做的动作。而内层事务操作失败并不会引起外层事务的回滚。

* 总结
  * 调用this方法(A调B，B抛错)
    * 无事务方法调用有事务方法时事务不生效
    * @Transaction实现基于AOP动态代理，this调用底层class本体对象，没有进入切点执行切面方法，所以事务不生效，改用代理对象调用即可A a;a.function();
  * 调用不同类的方法(A调B，B抛错)
    * 无事务方法调用有事务方法时,每穿过一层代理类新生成一个事务(A不回滚B回滚)
  * 捕获异常
    * 如果在抛错方法内捕获了异常，则此方法上的事务注解就感知不到这个异常的存在，事务不生效
```java
this
1.A->B(抛异常)	无
2.A->TB(抛异常)	无
3.TA->B(抛异常)	有
4.TA->TB(抛异常)有 

1.A		->B	捕	(抛异常)	无
2.A		->TB捕	(抛异常)	无
3.TA	->B	捕	(抛异常)	无
4.TA	->TB捕	(抛异常)	无 

1.A	捕	->B		(抛异常)	无	
2.A	捕	->TB	(抛异常)	无	
3.TA捕	->B		(抛异常)	无	
4.TA捕	->TB	(抛异常)	无 	


===================================
跨类
1.A->B(抛异常)	无
2.A->TB(抛异常)	A无B有
3.TA->B(抛异常)	有
4.TA->TB(抛异常)有 

1.A		->B	捕	(抛异常)	无
2.A		->TB捕	(抛异常)	无
3.TA	->B	捕	(抛异常)	无
4.TA	->TB捕	(抛异常)	无 

1.A	捕	->B		(抛异常)	无	
2.A	捕	->TB	(抛异常)	A无B有	
3.TA捕	->B		(抛异常)	无	
4.TA捕	->TB	(抛异常)	有 	
```
---
### **SpringBoot**  <span id = "springboot" />

---
### **SpringCloud**  <span id = "springcloud" />



