### 目录
* [Sql](#sql)
    * [Mysql](#mysql)
        * [Base](#mysqlBase)
        * [分库分表](#mysqlIndex)
    * [Oracle](#oracle)
        * [Base](#oracleBase)
    * [索引](#index)
    * [事务](#transaction)
        * [事务的特性](#ACID)
        * [事务隔离级别](#transactionLevel)
* [NoSql](#nosql)
    * [Redis](#redis)
        * [Redis锁](#redisLock)
        * [Redis三种模式](#redisModule)
        * [Redis持久化](#redisPersist)
    * [ES](#es)
        * [ES环境搭建](#esInstall)
        * [ES用法](#esUsage)
* [缓存](#cache)
    * [缓存处理流程](#cacheFlow)
    * [缓存击穿](#cacheBreakdown)
    * [缓存穿透](#cachePenetrate)
    * [缓存雪崩](#cacheAvalanche)
---
## **Sql**  <span id = "sql" />
### **Mysql**  <span id = "mysql" />
#### Base <span id = "mysqlBase" />
```
// 1.新增
// 单条
insert into 表名(字段名...) values(值...);
-- 批量插入1
insert into 表名(字段名...) values(值...),(值...);
-- 批量插入2(两张表的字段要一一对应)
INSERT INTO t 
  SELECT id,age,name from t_copy where id < 10;
// 2.删除
delete from 表名 where 条件;
// 3.修改
-- 修改单表数据
update 表名 set 字段名=修改值 where 条件;
-- 修改多表数据(mysql特有)
update t1 left t2 on t1.a = t2.a
 set t1.b=b,t2.c=c where 条件;
// 4.查询
select * from 表名;
```

#### 分库分表 <span id = "mysqlIndex" />  
出处：[MySQL 分库分表方案](https://www.cnblogs.com/aksir/p/9085694.html)
1. 分库分表前的问题  
   任何问题都是太大或者太小的问题，我们这里面对的数据量太大的问题。
   * 用户请求量太大  
       * 因为单服务器TPS，内存，IO都是有限的。   
     * 解决方法：分散请求到多个服务器上； 其实用户请求和执行一个sql查询是本质是一样的，都是请求一个资源，只是用户请求还会经过网关，路由，http服务器等。  
   * 单库太大  
     * 单个数据库处理能力有限；单库所在服务器上磁盘空间不足；单库上操作的IO瓶颈   
     * 解决方法：切分成更多更小的库  
   * 单表太大  
     * CRUD都成问题；索引膨胀，查询超时  
     * 解决方法：切分成多个数据集更小的表。  
2. 分库分表的方式方法  
   一般就是垂直切分和水平切分，这是一种结果集描述的切分方式，是物理空间上的切分;   
   * 垂直拆分  
     * 垂直分表  
       也就是“大表拆小表”，基于列字段进行的。一般是表中的字段较多，将不常用的， 数据较大，长度较长（比如text类型字段）的拆分到“扩展表“。一般是针对那种几百列的大表，也避免查询时，数据量太大造成的“跨页”问题。  
     * 垂直分库  
       垂直分库针对的是一个系统中的不同业务进行拆分，比如用户User一个库，商品Producet一个库，订单Order一个库。 切分后，要放在多个服务器上，而不是一个服务器上。为什么？ 我们想象一下，一个购物网站对外提供服务，会有用户，商品，订单等的CRUD。没拆分之前， 全部都是落到单一的库上的，这会让数据库的单库处理能力成为瓶颈。按垂直分库后，如果还是放在一个数据库服务器上， 随着用户量增大，这会让单个数据库的处理能力成为瓶颈，还有单个服务器的磁盘空间，内存，tps等非常吃紧。 所以我们要拆分到多个服务器上，这样上面的问题都解决了，以后也不会面对单机资源问题。
       数据库业务层面的拆分，和服务的“治理”，“降级”机制类似，也能对不同业务的数据分别的进行管理，维护，监控，扩展等。 数据库往往最容易成为应用系统的瓶颈，而数据库本身属于“有状态”的，相对于Web和应用服务器来讲，是比较难实现“横向扩展”的。 数据库的连接资源比较宝贵且单机处理能力也有限，在高并发场景下，垂直分库一定程度上能够突破IO、连接数及单机硬件资源的瓶颈。
   * 水平拆分
     * 水平分表  
        针对数据量巨大的单张表（比如订单表），按照某种规则（RANGE,HASH取模等），切分到多张表里面去。 但是这些表还是在同一个库中，所以库级别的数据库操作还是有IO瓶颈。不建议采用。  
     * 水平分库分表  
        将单张表的数据切分到多个服务器上去，每个服务器具有相应的库与表，只是表中数据集合不同。 水平分库分表能够有效的缓解单机和单库的性能瓶颈和压力，突破IO、连接数、硬件资源等的瓶颈。
     * 水平分库分表切分规则
       1. RANGE（范围）–这种模式允许将数据划分不同范围。例如可以将一个表通过年份划分成若干个分区。
        ```java
        // 将用户表分成4个分区，以每300万条记录为界限，每个分区都有自己独立的数据、索引文件的存放目录，与此同时，这些目录所在的物理磁盘分区可能也都是完全独立的，可以提高磁盘IO吞吐量。
        CREATE TABLE users (
           uid INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
           name VARCHAR(30) NOT NULL DEFAULT '',
           email VARCHAR(30) NOT NULL DEFAULT ''
        )
        PARTITION BY RANGE (uid) (
           PARTITION p0 VALUES LESS THAN (3000000)
           DATA DIRECTORY = '/data0/data'
           INDEX DIRECTORY = '/data1/idx',
        
           PARTITION p1 VALUES LESS THAN (6000000)
           DATA DIRECTORY = '/data2/data'
           INDEX DIRECTORY = '/data3/idx',
        
           PARTITION p2 VALUES LESS THAN (9000000)
           DATA DIRECTORY = '/data4/data'
           INDEX DIRECTORY = '/data5/idx',
        
           PARTITION p3 VALUES LESS THAN MAXVALUE
           DATA DIRECTORY = '/data6/data'
           INDEX DIRECTORY = '/data7/idx'
        );
        ```

       2. LIST(预定义列表)–这种模式允许系统通过预定义的列表的值来对数据进行分割。
        ```java
        CREATE TABLE category (
            cid INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(30) NOT NULL DEFAULT ''
        )
        PARTITION BY LIST (cid) (
            PARTITION p0 VALUES IN (0,4,8,12)
            DATA DIRECTORY = '/data0/data'
            INDEX DIRECTORY = '/data1/idx',
        
            PARTITION p1 VALUES IN (1,5,9,13)
            DATA DIRECTORY = '/data2/data'
            INDEX DIRECTORY = '/data3/idx',
            
            PARTITION p2 VALUES IN (2,6,10,14)
            DATA DIRECTORY = '/data4/data'
            INDEX DIRECTORY = '/data5/idx',
            
            PARTITION p3 VALUES IN (3,7,11,15)
            DATA DIRECTORY = '/data6/data'
            INDEX DIRECTORY = '/data7/idx'
        );
        ```

       3. HASH（哈希）–这中模式允许通过对表的一个或多个列的Hash Key进行计算，最后通过这个Hash码不同数值对应的数据区域进行分区。
        ```java
        // 基于给定的分区个数，将数据分配到不同的分区，HASH分区只能针对整数进行HASH，对于非整形的字段只能通过表达式将其转换成整数。
        // 表达式可以是mysql中任意有效的函数或者表达式，对于非整形的HASH往表插入数据的过程中会多一步表达式的计算操作，所以不建议使用复杂的表达式这样会影响性能。
        // MYSQL支持两种HASH分区，常规HASH(HASH)和线性HASH(LINEAR HASH) 。
        CREATE TABLE users (
            uid INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(30) NOT NULL DEFAULT '',
            email VARCHAR(30) NOT NULL DEFAULT ''
        )
        PARTITION BY HASH (uid) PARTITIONS 4 (
            PARTITION p0
            DATA DIRECTORY = '/data0/data'
            INDEX DIRECTORY = '/data1/idx',
            
            PARTITION p1
            DATA DIRECTORY = '/data2/data'
            INDEX DIRECTORY = '/data3/idx',
            
            PARTITION p2
            DATA DIRECTORY = '/data4/data'
            INDEX DIRECTORY = '/data5/idx',
            
            PARTITION p3
            DATA DIRECTORY = '/data6/data'
            INDEX DIRECTORY = '/data7/idx'
        );
        ```
       4. Key(键值)-Hash模式的一种延伸，这里的Hash Key是MySQL系统产生的。
        ```java
        // KEY分区和HASH分区相似，但是KEY分区支持除text和BLOB之外的所有数据类型的分区，而HASH分区只支持数字分区，
        // KEY分区不允许使用用户自定义的表达式进行分区，KEY分区使用系统提供的HASH函数进行分区。
        // 当表中存在主键或者唯一键时，如果创建key分区时没有指定字段系统默认会首选主键列作为分区字列,如果不存在主键列会选择非空唯一键列作为分区列,注意唯一列作为分区列唯一列不能为null。
        CREATE TABLE users (
            uid INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(30) NOT NULL DEFAULT '',
            email VARCHAR(30) NOT NULL DEFAULT ''
        )
        PARTITION BY KEY (uid) PARTITIONS 4 (
            PARTITION p0
            DATA DIRECTORY = '/data0/data'
            INDEX DIRECTORY = '/data1/idx',
            
            PARTITION p1
            DATA DIRECTORY = '/data2/data'
            INDEX DIRECTORY = '/data3/idx',
            
            PARTITION p2
            DATA DIRECTORY = '/data4/data'
            INDEX DIRECTORY = '/data5/idx',
            
            PARTITION p3
            DATA DIRECTORY = '/data6/data'
            INDEX DIRECTORY = '/data7/idx'
        );
        ```
       5. 子分区  
        ```java
        // 对 RANGE 分区再次进行子分区划分，子分区采用 HASH 类型。
        CREATE TABLE users (
            uid INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(30) NOT NULL DEFAULT '',
            email VARCHAR(30) NOT NULL DEFAULT ''
        )
        PARTITION BY RANGE (uid) SUBPARTITION BY HASH (uid % 4) SUBPARTITIONS 2(
            PARTITION p0 VALUES LESS THAN (3000000)
            DATA DIRECTORY = '/data0/data'
            INDEX DIRECTORY = '/data1/idx',
            
            PARTITION p1 VALUES LESS THAN (6000000)
            DATA DIRECTORY = '/data2/data'
            INDEX DIRECTORY = '/data3/idx'
        );
        ```


---
### **Oracle**  <span id = "oracle" />
#### Base <span id = "oracleBase" />
```
// 1.新增
// 单条
insert into 表名(字段名...) values(值...);
-- 批量插入1
insert into 表名(字段名...) values(值...),(值...);
-- 批量插入2(两张表的字段要一一对应)
INSERT INTO t 
  SELECT id,age,name from t_copy where id < 10;
// 2.删除
delete from 表名 where 条件;
// 3.修改
update 表名 set 字段名=修改值 where 条件;
// 4.查询
select * from 表名;
// 5.关键词 DISTINCT 用于返回唯一不同的值
SELECT DISTINCT 列名称 FROM 表名称;
```


---
### **索引**  <span id = "index" />
* 查询索引是否生效
``explain select... ``  
* 索引失效情况  
![](./img/DbIndex.png)


---
### **事务**  <span id = "transaction" />

#### **事务的特性**  <span id = "ACID" />
* 原子性（Atomicity）
    * 原子性是指事务包含的所有操作要么全部成功，要么全部失败回滚，因此事务的操作如果成功就必须要完全应用到数据库，如果操作失败则不能对数据库有任何影响。
* 一致性（Consistency）
    * 一致性是指事务必须使数据库从一个一致性状态变换到另一个一致性状态，也就是说一个事务执行之前和执行之后都必须处于一致性状态。
    * 拿转账来说，假设用户A和用户B两者的钱加起来一共是5000，那么不管A和B之间如何转账，转几次账，事务结束后两个用户的钱相加起来应该还得是5000，这就是事务的一致性。
* 隔离性（Isolation）
    * 隔离性是当多个用户并发访问数据库时，比如操作同一张表时，数据库为每一个用户开启的事务，不能被其他事务的操作所干扰，多个并发事务之间要相互隔离。
    * 即要达到这么一种效果：对于任意两个并发的事务T1和T2，在事务T1看来，T2要么在T1开始之前就已经结束，要么在T1结束之后才开始，这样每个事务都感觉不到有其他事务在并发地执行。
* 持久性（Durubility）
    * 持久性是指一个事务一旦被提交了，那么对数据库中的数据的改变就是永久性的，即便是在数据库系统遇到故障的情况下也不会丢失提交事务的操作。
    * 例如我们在使用JDBC操作数据库时，在提交事务方法后，提示用户事务操作完成，当我们程序执行完成直到看到提示后，就可以认定事务以及正确提交，即使这时候数据库出现了问题，也必须要将我们的事务完全执行完成，否则就会造成我们看到提示事务处理完毕，但是数据库因为故障而没有执行事务的重大错误。

#### **事务问题**  <span id = "transactionTrouble" />
* 脏读
  * 脏读是指在一个事务处理过程里读取了另一个未提交的事务中的数据。
* 不可重复读
  * 不可重复读是指在对于数据库中的某个数据，一个事务范围内多次查询返回了不同的数据值，这是由于在查询间隔，被另一个事务修改并提交了。
* 虚读(幻读)
  * 幻读是事务非独立执行时发生的一种现象。一个事务范围内多次查询返回了不同的集合。
* **注：**
    * 幻读不可重复读差不多，但幻读强调的集合的增减，不可重复读强调单独一条数据的修改。

#### **事务隔离级别**  <span id = "transactionLevel" />
* read uncommitted 未提交读
  * 所有事务都可以看到没有提交事务的数据。
* read committed 读已提交
  * 事务成功提交后才可以被查询到。
* repeatable 重复读
  * 同一个事务多个实例读取数据时，可能将未提交的记录查询出来，而出现幻读。
* Serializable可串行化
  * 强制的进行排序，在每个读读数据行上添加共享锁。会导致大量超时现象和锁竞争。  
    ![](./img/transactionLevel.png)

* 数据库隔离级别
  * Mysql : 支持上面四种隔离级别，默认的为Repeatable read (可重复读)；
  * Oracle : 只支持Read committed (读已提交)和Serializable (串行化)级别这两种级别，默认的为Read committed (读已提交)级别。

  
---
## **NoSql**  <span id = "nosql" />
### **Redis**  <span id = "redis" />
#### Redis锁  <span id = "redisLock" />
* [redis加锁的几种实现](https://blog.csdn.net/liuyongchen0824/article/details/88341981)
* redis能用的的加锁命令分表是INCR、SETNX、SET
#### Redis模式类型  <span id = "redisModule" />
* 主从模式、哨兵sentinel模式、redis cluster集群模式
* [redis三种模式对比](https://blog.csdn.net/selectgoodboy/article/details/86377861)
#### Redis持久化  <span id = "redisPersist" />
* [Redis 持久化之RDB和AOF](https://www.cnblogs.com/itdragon/p/7906481.html)
* RDB
  * RDB 是 Redis 默认的持久化方案。在指定的时间间隔内，执行指定次数的写操作，则会将内存中的数据写入到磁盘中。即在指定目录下生成一个dump.rdb文件。Redis 重启会通过加载dump.rdb文件恢复数据。
  * 触发RDB快照
  1. 在指定的时间间隔内，执行指定次数的写操作
  2. 执行save（阻塞， 只管保存快照，其他的等待） 或者是bgsave （异步）命令
  3. 执行flushall 命令，清空数据库所有数据，意义不大。
  4. 执行shutdown 命令，保证服务器正常关闭且不丢失任何数据，意义...也不大。
  * 通过RDB文件恢复数据
  * 将dump.rdb 文件拷贝到redis的安装目录的bin目录下，重启redis服务即可。在实际开发中，一般会考虑到物理机硬盘损坏情况，选择备份dump.rdb 。
  * RDB 的优缺点
  1. 优点：
  * 适合大规模的数据恢复。
  * 如果业务对数据完整性和一致性要求不高，RDB是很好的选择。
  2. 缺点：
  * 数据的完整性和一致性不高，因为RDB可能在最后一次备份时宕机了。
  * 备份时占用内存，因为Redis 在备份时会独立创建一个子进程，将数据写入到一个临时文件（此时内存中的数据是原来的两倍哦），最后再将临时文件替换之前的备份文件。所以Redis 的持久化和数据的恢复要选择在夜深人静的时候执行是比较合理的。
   * AOF
  * AOF ：Redis 默认不开启。它的出现是为了弥补RDB的不足（数据的不一致性），所以它采用日志的形式来记录每个写操作，并追加到文件中。Redis 重启的会根据日志文件的内容将写指令从前到后执行一次以完成数据的恢复工作。
  * 触发AOF快照
  1. 根据配置文件触发，可以是每次执行触发，可以是每秒触发，可以不同步。
  * 根据AOF文件恢复数据
  * 正常情况下，将appendonly.aof 文件拷贝到redis的安装目录的bin目录下，重启redis服务即可。但在实际开发中，可能因为某些原因导致appendonly.aof 文件格式异常，从而导致数据还原失败，可以通过命令redis-check-aof --fix appendonly.aof 进行修复 。
  * RDB 的优缺点
  1. 优点：数据的完整性和一致性更高
  2. 缺点：因为AOF记录的内容多，文件会越来越大，数据恢复也会越来越慢。

### **ES**  <span id = "es" />
#### ES环境搭建  <span id = "esInstall" />
* [ES基础环境搭建](https://blog.csdn.net/qq_19806865/article/details/121494891)

#### ES用法  <span id = "esUsage" />
```json
{
	"query": {
		"bool":{ //通过bool进行多条件的匹配查询
			"must":[ //must(相当于MySQL中的and)，所有条件都要符合
					//must_not(相当于MySQL中的!=)
					//should(相当于MySQL中的or)，所有条件或的查询
				{
					"bool":{
						"must":[ 
							{
								"match": { 
									"name": "Rel"
								}
							},
							{
								"match": { 
									"school": "foot"
								}
							}
						]
					}
				},
				{
					"bool":{ //通过bool进行多条件的匹配查询
						"must_not":[ 
							{
								"match": { 
									"age": 5
								}
							}
						]
					}
				}
			]
		},
		"filter": {
			"range": { // 范围查询
				"price": { 
					"gt": 400, // gt大于 gte大于等于 
					"lt": 700 // lt小于 lte小于等于
				}
			}
		}
	},
    "aggs": { // 函数查询
        "sum_number": { // 别名
            "sum": { // sum 求和 max 最大值 min 最小值 avg 平均值
						// stats 将记录数、最大最小、平均值、和都展示出来
                "field": "name"
            }
        },
		"unique_num": {
            "cardinality": { // 唯一值(类似于sql中的distinct)
								// terms 实现分组查询
                "field": "cust_name_s.keyword"
            }
        }
    }
}
```

---
## **缓存**  <span id = "cache" />
### **缓存处理流程**  <span id = "cacheFlow" />
* 前端请求，后端先从缓存中取数据，取到直接返回结果，取不到时从数据库中取，数据库取到更新缓存，并返回结果，数据库也没取到，那直接返回空结果。
* ![](./img/cacheFlow.png)  

### **缓存击穿**  <span id = "cacheBreakdown" />
* 描述:
    * 缓存击穿是指缓存中没有但数据库中有的数据（一般是缓存时间到期），这时由于并发用户特别多，同时读缓存没读到数据，又同时去数据库去取数据，引起数据库压力瞬间增大，造成过大压力
* 解决方案：
    1. 设置热点数据永远不过期。
    2. 缓存加互斥锁，等待查询到数据写入缓存后返回缓存数据。

### **缓存穿透**  <span id = "cachePenetrate" />
* 描述:
    * 缓存穿透是指缓存和数据库中都没有的数据，而用户不断发起请求，如发起为id为“-1”的数据或id为特别大不存在的数据。这时的用户很可能是攻击者，攻击会导致数据库压力过大。
* 解决方案：
    1. 接口层增加校验，如用户鉴权校验，id做基础校验，id<=0的直接拦截；
    2. 从缓存取不到的数据，在数据库中也没有取到，这时也可以将key-value对写为key-null，缓存有效时间可以设置短点，如30秒（设置太长会导致正常情况也没法使用）。这样可以防止攻击用户反复用同一个id暴力攻击

### **缓存雪崩**  <span id = "cacheAvalanche" />
* 描述:
    * 缓存雪崩是指缓存中数据大批量到过期时间，而查询数据量巨大，引起数据库压力过大甚至down机。 
    * 和缓存击穿不同的是，缓存击穿指并发查同一条数据，缓存雪崩是不同数据都过期了，很多数据都查不到从而查数据库。
    * 比较致命的缓存雪崩，是缓存服务器某个节点宕机或断网
* 解决方案：
    1. 缓存数据的过期时间设置随机，防止同一时间大量数据过期现象发生。
    2. 如果缓存数据库是分布式部署，将热点数据均匀分布在不同的缓存数据库中。
    3. 设置热点数据永远不过期。




