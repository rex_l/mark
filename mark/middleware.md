### 目录
* [RabbitMQ](#rabbitMQ)
    * [基本消息模型](#baseMessage)
    * [工作队列](#workQueues)
    * [订阅模型](#subscribe)
* [Nginx](#nginx)
    * [路径地址加不加“/”的作用](#nginxLocation)
    * [负载均衡实现](#nginxBalance)
---
### **RabbitMQ**  <span id = "rabbitMQ" />
[RabbitMQ使用详解](https://www.cnblogs.com/ithushuai/p/12443460.html)
#### 定义  <span id = "thread" />
> RabbitMQ是基于AMQP的一款消息管理系统。AMQP(Advanced Message Queuing Protocol)，是一个提供消息服务的应用层标准高级消息队列协议，其中RabbitMQ就是基于这种协议的一种实现。

#### 基本消息模型  <span id = "baseMessage" />
* 生产者将消息发送到队列，消费者从队列中获取消息，队列是存储消息的缓冲区。  
![](./img/baseMessage.png)
  
#### 工作队列  <span id = "workQueues" />
* 在基本消息模型中，一个生产者对应一个消费者，而实际生产过程中，往往消息生产会发送很多条消息，如果消费者只有一个的话效率就会很低，因此rabbitmq有另外一种消息模型，这种模型下，一个生产发送消息到队列，允许有多个消费者接收消息，但是一条消息只会被一个消费者获取。  
![](./img/workQueues.png)
* 工作队列模型原理同基本消息模型相同
  1. 基本消息模型1生产者->1消费者
  2. 工作队列模型1生产者->n消费者
     (n个消费者监听同一个队列，当n消费者都工作时，生产者发送消息，会按照负载均衡算法分配给不同消费者)  
     ![](./img/workQueuesDemo.png)
     
#### 订阅模型  <span id = "subscribe" />
* 订阅模式中，可以实现一条消息被多个消费者获取。在这种模型下，消息传递过程中比之前多了一个exchange交换机，生产者不是直接发送消息到队列，而是先发送给交换机，经由交换机分配到不同的队列，而每个消费者都有自己的队列：  
  ![](./img/subscribe.png)
1. 1个生产者，多个消费者
2. 每一个消费者都有自己的一个队列
3. 生产者没有将消息直接发送到队列，而是发送到了交换机
4. 每个队列都要绑定到交换机
5. 生产者发送的消息，经过交换机到达队列，实现一个消息被多个消费者获取的目的

*   **注意：在发布订阅模型中，生产者只负责发消息到交换机，至于消息该怎么发，以及发送到哪个队列，生产者都不负责。一般由消费者创建队列，并且绑定到交换机**

* X（exchange）交换机的类型有以下几种：
    * Fanout：广播，交换机将消息发送到所有与之绑定的队列中去
    * Direct：定向，交换机按照指定的Routing Key发送到匹配的队列中去
    * Topics：通配符，与Direct大致相同，不同在于Routing Key可以根据通配符进行匹配

---
### **Nginx**  <span id = "nginx" />
#### 路径地址加不加“/”的作用  <span id = "nginxLocation" />
1. proxy_pass(ip:port) -> proxy_pass + location + 请求URL除去location之后的部分
2. "*" -> proxy_pass + 请求URL除去location之后的部分

```
配置1：拦截转发都有斜杠
location /demo/ {
   proxy_pass http://127.0.0.1/;
}
> 请求：http://127.0.0.1/demo/api/abc
> 转发：http://127.0.0.1/api/abc
```
```
配置2：拦截没有斜杠，转发有斜杠
location /demo {
   proxy_pass http://127.0.0.1/;
}
> 请求：http://127.0.0.1/demo/api/abc
> 转发：http://127.0.0.1//api/abc
```
```
配置3：拦截有斜杠，转发没有斜杠
location /demo/ {
   proxy_pass http://127.0.0.1;
}
> 请求：http://127.0.0.1/demo/api/abc
> 转发：http://127.0.0.1/demo/api/abc
```
```
配置4：拦截转发都没有斜杠
location /demo {
   proxy_pass http://127.0.0.1;
}
> 请求：http://127.0.0.1/demo/api/abc
> 转发：http://127.0.0.1/demo/api/abc
```
```
配置5：拦截转发都有斜杠（转发URl包含至少一级目录）
location /demo/ {
   proxy_pass http://127.0.0.1/server/;
}
> 请求：http://127.0.0.1/demo/api/abc
> 转发：http://127.0.0.1/server/api/abc
```
```
配置6：拦截没有斜杠，转发有斜杠（转发URl包含至少一级目录）
location /demo {
   proxy_pass http://127.0.0.1/server/;
}
> 请求：http://127.0.0.1/demo/api/abc
> 转发：http://127.0.0.1/server//api/abc
```
```
配置7：拦截有斜杠，转发没有斜杠（转发URl包含至少一级目录）
location /demo/ {
   proxy_pass http://127.0.0.1/server;
}
> 请求：http://127.0.0.1/demo/api/abc
> 转发：http://127.0.0.1/serverapi/abc
```
```
配置8：拦截转发都没有斜杠（转发URl包含至少一级目录）
location /demo {
   proxy_pass http://127.0.0.1:8080/server;
}
> 请求：http://127.0.0.1/demo/api/abc
> 转发：http://127.0.0.1/server/api/abc
```

---
#### 负载均衡实现  <span id = "nginxBalance" />
1. 普通轮询算法
  ```java
    // 配置的ip之间进行交替访问
    upstream OrdinaryPolling {
      server 127.0.0.1:8080;
      server 127.0.0.1:8081;
    }
    server {
      listen       80;
      server_name  localhost;
  
      location / {
        proxy_pass http://OrdinaryPolling;
        index  index.html index.htm index.jsp;
      }
    }
  ```
2. 基于比例加权轮询
  ```java
    // 加权轮询机制，权重越大访问的请求相对越多
    upstream OrdinaryPolling {
      server 127.0.0.1:8080 weight=5;
      server 127.0.0.1:8081 weight=2;
    }
    server {
      listen       80;
      server_name  localhost;
  
      location / {
        proxy_pass http://OrdinaryPolling;
        index  index.html index.htm index.jsp;
      }
    }
  ```
3. 基于IP路由负载
  ```java
    // 根据IP计算，相同的IP访问相同的服务器，解决用户session问题
    upstream OrdinaryPolling {
      ip_hash;
      server 127.0.0.1:8080 weight=5;
      server 127.0.0.1:8081 weight=2;
    }
    server {
      listen       80;
      server_name  localhost;
  
      location / {
        proxy_pass http://OrdinaryPolling;
        index  index.html index.htm index.jsp;
      }
    }
  ```
4. 基于服务器响应时间负载分配
  ```java
    // 根据服务器处理请求的时间来进行负载，处理请求越快，也就是响应时间越短的优先分配
    upstream OrdinaryPolling {
      server 127.0.0.1:8080 weight=5;
      server 127.0.0.1:8081 weight=2;
      fair;
    }
    server {
      listen       80;
      server_name  localhost;
  
      location / {
        proxy_pass http://OrdinaryPolling;
        index  index.html index.htm index.jsp;
      }
    }
  ```
5. 对不同域名实现负载均衡
  ```java
    // 通过配合location 指令块我们还可以实现对不同域名实现负载均衡。
    upstream wordbackend {
      server 127.0.0.1:8080;
      server 127.0.0.1:8081;
    }
    upstream pptbackend {
      server 127.0.0.1:8082;
      server 127.0.0.1:8083;
    }
    server {
      listen       80;
      server_name  localhost;
  
      location /word/ {
        proxy_pass http://wordbackend;
        index  index.html index.htm index.jsp;
      }
      location /ppt/ {
        proxy_pass http://pptbackend;
        index  index.html index.htm index.jsp;
      }
    }
  ```




